import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ForecastService {

  public data = {
    city_name: '',
    country_code: '',
    state_code: ''
  }
  public array: any[] = [];

  searched: boolean = false;

  constructor() { }

  public setSearched() {
    this.searched ? this.searched = false : this.searched = true;
  }

}
