import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import { ForecastService } from '../forecast/forecast.service';
import { DatePipe } from '@angular/common';
import { FormControl, Validators, NgModel, FormGroup } from '@angular/forms';

interface Cities {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})


export class SearchComponent implements OnInit {

  city: FormControl;

  private url = "http://localhost:3000/weather"

  cities: Cities[] = [
    {value: 'minneapolis', viewValue: 'Minneapolis'},
    {value: 'jackson', viewValue: 'Jackson'},
    {value: 'kansas', viewValue: 'Kansas'},
    {value: 'new York', viewValue: 'New York'},
    {value: 'other', viewValue: 'Other'}
  ];

  selected = '';
  search = '';
  written = '';

  error: boolean;

  constructor(private http: HttpClient,
              public forecastService: ForecastService,
              private datePipe: DatePipe) {
    this.error = false;
    this.city = new FormControl('', [Validators.required]);
  }

  ngOnInit(): void {
  }

  searchCity() {
    this.error = false;
    this.selected === 'other' ? this.search = this.written : this.search = this.selected

    if (this.search !== ''){
      this.http.post(this.url,
      {
        "city": this.search
      }).subscribe(
          (val:any) => {
            if (val) {
              console.log("POST call successful value returned in body", val);
              this.forecastService.setSearched();
              this.forecastService.data = {city_name: val.city_name,
                                           country_code: val.country_code,
                                           state_code: val.state_code }
              this.forecastService.array = val.data;

              this.selected = '';
              this.written = '';
            } else {
              this.error = true;
            }

          },
          response => {
              console.log("POST call in error", response);
          });
    } else {
      this.error = true;
    }
    }

}
