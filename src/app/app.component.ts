import { Component } from '@angular/core';
import { ForecastService } from './forecast/forecast.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'weather';

  constructor(public forecastService: ForecastService) { }
}
